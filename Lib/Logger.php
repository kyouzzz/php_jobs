<?php
namespace Lib;

use Lib\Config;

class Logger
{

    public static function info($message)
    {
        self::write('info', $message);
    }

    public static function warn($message)
    {
        self::write('warn', $message);
    }

    public static function error($message)
    {
        self::write('error', $message);
    }

    private static function write($level, $message)
    {
        $file = Config::get("log_path", 'common');
        if (!$file) {
            return false;
        }
        $content = date('Y-m-d H:i:s') . " [$level] " . $message . PHP_EOL;
        file_put_contents($file, $content, FILE_APPEND);
    }

}
