<?php
namespace Lib;

class Config
{

    public static function get($key, $file = "common")
    {
        $config_paths = json_decode(CONFIG_PATH);
        // 所以,后面的 config 会覆盖前面的
        foreach ($config_paths as $value) {
            $file_name = $file . ".php";
            $config_file = $value . $file_name;
            if (file_exists($config_file)) {
                include($config_file);
            }
        }

        if (isset($config[$key])) {
            return $config[$key];
        } else {
            // TODO LOG WARN
            // 未找到配置文件 记录警告信息
            return '';
        }
    }

}
