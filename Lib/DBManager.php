<?php
namespace Lib;

use Lib\Config;

class DBManager
{
    private static $pdo_list;

    private function __construct($db_name)
    {
        $db = Config::get("db", 'database');
        $conf = $db[$db_name];
        if (!$conf) {
            throw new Exception("Can't find $db_name DB config.");
        }
        $host = $conf['host'];
        $port = empty($conf['port']) ? 3306 : $conf['port'];
        $user = $conf['name'];
        $password = $conf['password'];
        $dbname = $conf['database'];
        try {
            // cache pdo
            $new_pdo = new \PDO(
                "mysql:host=$host;dbname=$dbname;port=$port",
                $user,
                $password);
            $new_pdo->query("SET NAMES utf8");
            $new_pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            self::$pdo_list[$db_name] = $new_pdo;
        } catch (\PDOException $e) {
            print_r($e->getMessage());
        }
    }

    public static function factory($db_name)
    {
        if (empty(self::$pdo_list[$db_name])) {
            new self($db_name);
        }
        return self::$pdo_list[$db_name];
    }

}
