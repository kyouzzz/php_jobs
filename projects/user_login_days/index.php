<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class UserLoginDays
{
    public function run()
    {
        $users_txt = fopen("/tmp/users.txt", "r");
        $out_csv = fopen("/tmp/users_days.csv", "a+");
        $i = 0;
        while (!feof($users_txt)) {
            $i ++;
            $line = trim(fgets($users_txt));
            if (!$line) {
                continue ;
            }
            $user_arr = split(" +",$line);
            $csv_column = [];
            foreach ($user_arr as $gid) {
                $account_id = $this->getAccountId($gid);
                if (!$account_id) {
                    Logger::error(get_class() . ": Not find account_id.");
                    continue;
                }
                $last_year_days = $this->getUserLastYearDays($account_id);
                $this_year_days = $this->getUserThisYearDays($account_id);
                $csv_column[] = $gid;
                $csv_column[] = $last_year_days;
                $csv_column[] = $this_year_days;
            }
            fputcsv($out_csv, $csv_column);
            echo "Runing line:" . $i .PHP_EOL;
        }
        Logger::info(get_class() . ": job run finish.");
    }

    public function run_ext()
    {
        $users_txt = fopen("/tmp/users.txt", "r");
        $out_csv = fopen("/tmp/users_days_3.csv", "a+");
        $i = 0;
        $exist_gids = [];
        while (!feof($users_txt)) {
            $line = trim(fgets($users_txt));
            $user_arr = split(" +",$line);
            foreach ($user_arr as $gid) {
                if (!in_array($gid, $exist_gids)) {
                    $exist_gids[] = $gid;
                }
            }
        }

        $micro_db = DBManager::factory("hon_micro");
        $curr_account_id = 0;
        $num = 0;
        $has_more = true;
        while ($has_more) {
            $account_stmt = $micro_db->prepare("select * from hon_accounts.account where account_id > :start_id order by account_id asc limit 1000");
            $account_stmt->execute(['start_id'=> $curr_account_id]);
            $accounts = $account_stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!$accounts) {
                $has_more = false;
            }
            foreach ($accounts as $account) {
                $csv_column = [];
                $garena_id = $account['garena_id'];
                $curr_account_id = $account['account_id'];
                $account_id = $curr_account_id;
                if (in_array($garena_id, $exist_gids)) {
                    continue;
                }
                if(!$this->checkLoginIn($account_id)) {
                    $num ++;
                    $last_year_days = $this->getUserLastYearDays($account_id);
                    $this_year_days = $this->getUserThisYearDays($account_id);
                    $csv_column[] = $garena_id;
                    $csv_column[] = $last_year_days;
                    $csv_column[] = $this_year_days;
                    fputcsv($out_csv, $csv_column);
                    echo "get user $garena_id: " . $num . PHP_EOL;
                }
            }
            sleep(1);
        }
        Logger::info(get_class() . ": job run finish.");

    }

    private function getAccountId($garena_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $account_stmt = $micro_db->prepare("select * from hon_accounts.account where garena_id=:id");
        $account_stmt->execute(['id'=> $garena_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        if (!$account) {
            return null;
        }
        return $account['account_id'];
    }

    public function getUserLastYearDays($account_id)
    {
        $start_date = "2016-05-25";
        $end_date = "2016-06-25";
        return $this->getUserLoginDays($account_id, $start_date, $end_date, true);
    }
    public function getUserThisYearDays($account_id)
    {
        $start_date = "2017-05-29";
        $end_date = "2017-06-28";
        return $this->getUserLoginDays($account_id, $start_date, $end_date, false);
    }
    private function getUserLoginDays($account_id, $start_date, $end_date, $use_bak)
    {
        if (!$use_bak) {
            $table = "logins";
            $db = "hon_tracking";
        } else {
            $table = "logins_20161020";
            $db = "hon_tracking_bak";
        }
        $micro_db = DBManager::factory($db);
        $login_stmt = $micro_db->prepare("select count(distinct date) as days from $table where account_id=:account_id and `date`>=:start_date and `date`<=:end_date");
        $login_stmt->execute(['account_id'=> $account_id, "start_date" => $start_date, "end_date" => $end_date]);
        $result = $login_stmt->fetch(PDO::FETCH_ASSOC);
        return $result['days'];
    }

    private function checkLoginIn($account_id)
    {
        $start_date = "2016-04-10";
        $end_date = "2016-05-05";
        $days = $this->getUserLoginDays($account_id, $start_date, $end_date, true);
        return $days;
    }
    
}

$clazz = new UserLoginDays();
$clazz->run_ext();