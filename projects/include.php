<?php

define("ROOT_PATH", dirname(dirname(__FILE__)));
define("JOB_PATH", ROOT_PATH . "/projects");
define("CONFIG_PATH", json_encode([
        // local config
        JOB_PATH . "/config/",
        // live config
        //"/tmp/config_test/",
    ])
);
$namespaces = array(
    'Lib\\' => ROOT_PATH . '/Lib',
);

foreach ($namespaces as $namespace => $classpath) {
    spl_autoload_register(function ($classname) use ($namespace, $classpath) {
        // Check if the namespace matches the class we are looking for
        if (preg_match("#^".preg_quote($namespace)."#", $classname)) {
            // Remove the namespace from the file path since it's psr4
            $classname = str_replace($namespace, "", $classname);
            $filename = preg_replace("#\\\\#", "/", $classname).".php";
            include_once $classpath."/$filename";
        }
    });
}
