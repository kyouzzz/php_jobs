<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class CheckPlayerProducts
{
    private $micro_db;

    public function run()
    {
        $micro_db = DBManager::factory("hon_micro");
        $elig_products = [4403,2540,3017,3021,2905,2627,2339,3318,1999,3453,983,4736,4735,1771,1769];

        foreach ($elig_products as $id) {
            $this->checkEligProduct($id);
        }
        Logger::info(get_class() . ': job run finish.');
    }

    private function checkEligProduct($id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $stmt = $micro_db->prepare("select * from `products_eligibility` e left join `products_required` r using (elig_id) where e.product_id=:id");
        $stmt->execute(['id'=> $id]);
        $elig_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $required_products = [];
        foreach ($elig_result as $value) {
            $required_products[] = $value['product_id'];
        }
        if (empty($required_products)) {
            Logger::error(get_class() . ": None required products in $id.");
        }
        $trans_smt = $micro_db->prepare("select * from transactions where product_id=:id and date >='2017-06-01' and status=1");
        $trans_smt->execute(['id'=> $id]);
        $consume_players = $trans_smt->fetchAll(PDO::FETCH_ASSOC);

        $file = fopen('/tmp/elig_player.csv', 'a+');
        foreach ($consume_players as $value) {
            $super_id = $value['super_id'];
            Logger::info(get_class() . ": running product_id $id ; Player super_id $super_id .");
            $missed_ids = $this->getMissedProducts($super_id, $required_products);
            if ($missed_ids) {
                $account_stmt = $micro_db->prepare("select * from hon_accounts.account where super_id=:id");
                $account_stmt->execute(['id'=> $super_id]);
                $account = $account_stmt->fetch(PDO::FETCH_ASSOC);

                fputcsv($file, [$id,$account['garena_id'],$account['nickname'],implode(" ", $required_products)]);
            }
        }
    }

    private function getMissedProducts($super_id, $required_ids)
    {
        $micro_db = DBManager::factory("hon_micro");
        $in_str = implode(",", $required_ids);
        $player_products_stmt = $micro_db->prepare("select * from upgrades where super_id=:super_id and product_id in ($in_str)");
        $player_products_stmt->execute(['super_id'=> $super_id]);
        $player_products = $player_products_stmt->fetchAll(PDO::FETCH_ASSOC);
        $player_product_ids = [];
        foreach ($player_products as $value) {
            $player_product_ids[] = $value['product_id'];

        }
        return array_diff($required_ids, $player_product_ids);
    }


}
$clazz = new CheckPlayerProducts();
$clazz->run();

