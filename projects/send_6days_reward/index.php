<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class SendRewards
{
    private $micro_db;

    public function run()
    {
        $micro_db = DBManager::factory("hon_micro");
        $stmt = $micro_db->prepare("select super_id from transactions where product_id=4767");
        $stmt->execute();
        $check_users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $num = 0;
        foreach ($check_users as $user) {
            $res = $this->checkUser($user['super_id']);
            if ($res) {
                $num ++;
                try {
                    $this->sendReward($user['super_id']);
                    file_put_contents("/tmp/success_users.log", $user['super_id'] . PHP_EOL , FILE_APPEND);
                } catch (\Exception $e) {
                    file_put_contents("/tmp/fail_users.log", $user['super_id'] . PHP_EOL , FILE_APPEND);
                }
                usleep(200000); // sleep 200ms
            }
        }
        Logger::info(get_class() . ": job run finish. $num");
    }

    public function checkUser($super_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $check_products = [4765,4766,4767,4768,4769,4770];
        $in_str = implode(",", $check_products);
        $player_products_stmt = $micro_db->prepare("select * from upgrades where super_id=:super_id and product_id in ($in_str)");
        $player_products_stmt->execute(['super_id'=> $super_id]);
        $player_products = $player_products_stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($player_products) == 6) {
            return true;
        }
        return false;
    }

    public function sendReward($super_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        // find account
        $account_stmt = $micro_db->prepare("select * from hon_accounts.account where super_id=:id");
        $account_stmt->execute(['id'=> $super_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        $account_id = $account['account_id'];

        // give super boost * 2 4605
        $table_name = "player_limited_inventory_" . ($account_id % 10);
        $inv_query_stmt = $micro_db->prepare("select * from $table_name where account_id=:id and product_id=4605");
        $inv_query_stmt->execute(['id'=> $account_id]);
        $inv_query_result = $inv_query_stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($inv_query_result) {
            // update exist
            $sql = "update $table_name set quantity=quantity+2 where account_id=$account_id and product_id=4605";
        } else {
            // add new
            $sql = "insert into $table_name (account_id,product_id,limit_type,quantity,start_time,end_time,discount,mmp_discount) values ($account_id,4605,2,2,NULL,NULL,1,1)";
        }
        Logger::info(get_class() . "[ADD SUPER BOOST SQL]: $sql");
        $add_product_stmt = $micro_db->prepare($sql);
        $add_product_stmt->execute();
        $add_product_result = $add_product_stmt->rowCount();

        // give 66 coins
        $points_stmt = $micro_db->prepare("select * from acc2point where super_id=:id");
        $points_stmt->execute(['id'=> $super_id]);
        $points = $points_stmt->fetch(PDO::FETCH_ASSOC);
        if ($points) {
            $update_points_sql = "update acc2point set points=points+66 where super_id=$super_id";
            $points_update_stmt = $micro_db->prepare($update_points_sql);
            $points_update_stmt->execute();
            $points_update_result = $points_update_stmt->rowCount();
            Logger::info(get_class() . "[ADD POINTS SQL]: $update_points_sql");
        } else {
            Logger::error(get_class() . "[ADD POINTS ERROR]: Not find acc2point $super_id");
        }
        $curr_points = $points['points'] + 66;
        $curr_mmpoints = $points['mmpoints'];
        
        // add super boost transcations
        $date = date("Y-m-d");
        $time = date("H:i:s");
        if ($add_product_result) {
            $trans_sql = "insert into transactions (super_id,product_id,status,spent,mmspent,`date`,`time`,current_points_balance,current_mmpoints_balance) values ($super_id,4605,1,0,0,'{$date}','{$time}',$curr_points,$curr_mmpoints)";
            $add_trans_stmt = $micro_db->prepare($trans_sql);
            $add_trans_stmt->execute();
            $add_trans_result = $add_trans_stmt->rowCount();
            if ($add_trans_result) {
                Logger::info(get_class() . "[ADD TRANS SQL]: $trans_sql");
            } else {
                Logger::error(get_class() . "[ADD TRANS ERROR]: $trans_sql");
            }
        } else {
            Logger::error(get_class() . "[ADD SUPER BOOST ERROR]: $super_id");
        }

        // add invoice
        $add_invoice_sql = "insert into invoice (super_id,gifter_id,gifter_nick,points,total,paygw_trans_id,`date`,`time`,complete,`type`,`first_time`,`current_points_balance`,`current_mmpoints_balance`) values ($super_id,0,'php*',66,0,'InGameCheckInEvent#2:{$super_id}','{$date}','{$time}',1,'free',0,$curr_points,$curr_mmpoints)";
        $add_invoice_stmt = $micro_db->prepare($add_invoice_sql);
        $add_invoice_stmt->execute();
        $add_invoice_result = $add_invoice_stmt->rowCount();
        if ($add_invoice_result) {
            Logger::info(get_class() . "[ADD INVOICE SQL]: $add_invoice_sql");
        } else {
            Logger::error(get_class() . "[ADD INVOICE ERROR]: $add_invoice_sql");
        }

    }
}

$clazz = new SendRewards();
$clazz->run();