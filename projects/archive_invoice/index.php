<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;

class Archive
{
    private $micro_db;
    private $archive_db;

    public function run()
    {
        $micro_db = DBManager::factory("hon_micro");
        $date_last = date("Y-m-d H:i:s", strtotime("-1 years"));

        $has_more = true;
        while ($has_more) {
            $stmt = $micro_db->prepare("select * from invoice where date < :last_year  limit 100");
            $stmt->execute(['last_year'=> $date_last]);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                foreach ($result as $value) {
                    $this->transData($value);
                }
            } else {
                $has_more = false;
            }
        }
        echo "Finish.";
    }

    public function transData($data)
    {
        $archive_db = DBManager::factory("hon_micro_archive");
        $micro_db = DBManager::factory("hon_micro");
        // insert archive
        $sql = "insert into invoice_archive (invoice_id,super_id,gifter_id,gifter_nick,points,total,paygw_trans_id,`date`,`time`,complete,`type`,`sales_invoice_id`,`package_id`,`first_time`,`current_points_balance`,`current_mmpoints_balance`) values (:INVOICE_ID,:SUPER_ID,:GIFTER_ID,:GIFTER_NICK,:POINTS,:TOTAL,:PAYGW_TRANS_ID,:DATE,:TIME,:COMPLETE,:TYPE,:SALES_INVOICE_ID,:PACKAGE_ID,:FIRST_TIME,:CURRENT_POINTS_BALANCE,:CURRENT_MMPOINTS_BALANCE)";
        $mapping = [];
        foreach ($data as $key => $value) {
            $upper_key = strtoupper($key);
            $mapping[$upper_key] = $value;
        }
        $archive_stmt = $archive_db->prepare($sql);
        if($archive_stmt && $archive_stmt->execute($mapping)){
            // remove invoice
            $delete_stmt = $micro_db->prepare("delete from invoice where invoice_id = {$data['invoice_id']}");
            $delete_stmt->execute();
        }
    }

}
$clazz = new Archive();
$clazz->run();
