<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class ScoreQuery
{

    public function run($start_time, $end_time)
    {
        // 查询拥有miku的玩家
        $players = $this->getMikuPlayers();
        $out_file = fopen("/tmp/miku.csv", "a+");
        fputcsv($out_file, ['UID', 'Miku time', 'Points', 'Score']);
        $i = 0;
        foreach ($players as $value) {
            $super_id = $value['super_id'];
            $has_miku_time = $value['date_time'];
            $account = $this->getAccount($super_id);
            $garena_id = $account['garena_id'];
            $account_id = $account['account_id'];

            // 查询金币消耗
            // transaction 金币消耗
            $points = $this->getPlayerPoints($super_id, $start_time, $end_time);
            // 涂鸦购买统计
            $graffiti_times = $this->getPlayerGraffiti($account_id, $start_time, $end_time);
            // 计算积分
            $points = $points + $graffiti_times * 299 ;
            $score = ceil($points/1000) + 1;
            fputcsv($out_file, [$garena_id, $has_miku_time, $points, $score]);
            $i++;
            echo "Run num $i $garena_id $score" . PHP_EOL;
        }
        Logger::info(get_class() . ": job run finish.");

    }

    private function getMikuPlayers()
    {
        $micro_db = DBManager::factory("hon_micro");
        $trans_stmt = $micro_db->prepare("select distinct(super_id) super_id, CONCAT(date,' ',time) date_time from transactions where product_id=4437 and status=1");
        $trans_stmt->execute();
        $players = $trans_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $players;
    }

    public function getAccount($super_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $account_stmt = $micro_db->prepare("select super_id,garena_id,account_id from hon_accounts.account where super_id=:id");
        $account_stmt->execute(['id' => $super_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        return $account;
    }

    private function getPlayerPoints($super_id, $start_time, $end_time)
    {
        $micro_db = DBManager::factory("hon_micro");
        $trans_stmt = $micro_db->prepare("select sum(spent) spent from transactions where CONCAT(date,' ',time) >=:start_time and CONCAT(date,' ',time) <=:end_time and status=1 and super_id=:super_id");
        $trans_stmt->execute(['super_id' => $super_id, 'start_time' => $start_time, 'end_time' => $end_time]);
        $total = $trans_stmt->fetch(PDO::FETCH_ASSOC);
        return $total['spent'];
    }

    private function getPlayerGraffiti($account_id, $start_time, $end_time)
    {
        $start_time = microtime($start_time);
        $end_time = microtime($end_time);
        $micro_db = DBManager::factory("hon_micro");
        $buy_stmt = $micro_db->prepare("select count(1) times from event_graffiti_buy where create_time >=:start_time and create_time <=:end_time and account_id=:account_id");
        $buy_stmt->execute(['account_id' => $account_id, 'start_time' => $start_time, 'end_time' => $end_time]);
        $total = $buy_stmt->fetch(PDO::FETCH_ASSOC);
        return $total['times'];
    }

}

$clazz = new ScoreQuery();
$clazz->run("2017-08-01 13:00:00", "2017-08-31 13:00:00");
