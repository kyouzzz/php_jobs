<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class PlayerProductCheck
{
    // 检查的商品
    private $check_product_arr = [4515,4517,4519,4539,4541,4575,4576,4926,4928,4942,4924,4898,4857,4779,4777,4747,4507,4493,4491];

    public function run()
    {
        $out_csv = fopen("/tmp/users_avatars.csv", "a+");
        fputcsv($out_csv, array_merge(['garena_id'], $this->check_product_arr));

        $handle = fopen("data/super_ids.txt","r");
        $num = 0;
        while (!feof($handle)) {
            $line = trim(fgets($handle));
            if (!$line) {
                continue ;
            }
            $num ++;
            echo "Num $num" . PHP_EOL;
            $super_id = $line;
            $result = $this->handlePlayers($super_id);
            fputcsv($out_csv, $result);
        }
        Logger::info(get_class() . ": job run finish.");
    }

    public function handlePlayers($super_id)
    {
        $account = $this->getAccount($super_id);
        $result = [$account['garena_id']];

        $player_products = [];
        $in_str = implode(",", $this->check_product_arr);
        $micro_db = DBManager::factory("hon_micro");
        $player_products_stmt = $micro_db->prepare("select * from upgrades where super_id=:super_id and product_id in ($in_str)");
        $player_products_stmt->execute(['super_id'=> $super_id]);
        $player_products = $player_products_stmt->fetchAll(PDO::FETCH_ASSOC);

        $has_products = [];
        foreach ($player_products as $value) {
            $has_products[] = $value['product_id'];
        }
        foreach ($this->check_product_arr as $product_id) {
            if (in_array($product_id, $has_products)) {
                $result[] = 1;
                echo "$super_id has $product_id" . PHP_EOL;
            } else {
                $result[] = 0;
            }
        }
        return $result;
    }

    public function getAccount($super_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $account_stmt = $micro_db->prepare("select super_id,garena_id,account_id from hon_accounts.account where super_id=:id");
        $account_stmt->execute(['id' => $super_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        return $account;
    }

}

$clazz = new PlayerProductCheck();
$clazz->run();