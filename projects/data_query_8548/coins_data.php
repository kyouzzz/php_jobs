<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class CoinsData
{

    public function run()
    {
        $fun_arr = [
            "getAccount1" => 
            [
                "in_file" => "/tmp/users_0824.csv",
                "out_file" => "/tmp/data_0824.csv",
                "sep_date" => "2017-02-17",
                "date_range" =>[
                    ['2016-12-18','2017-01-17'],
                    ['2017-01-18','2017-02-17'],
                    ['2017-02-18','2017-03-20'],
                    ['2017-03-21','2017-04-20'],
                    ['2017-04-21','2017-05-21'],
                    ['2017-05-22','2017-06-21'],
                    ['2017-06-22','2017-07-22'],
                ]
            ],
            // "getAccount2" => 
            // [
            //     "in_file" => "/tmp/users_0428.csv",
            //     "out_file" => "/tmp/data_0428.csv",
            //     "sep_date" => "2017-04-28",
            //     "date_range" =>[['2017-03-28','2017-04-27'],['2017-04-28','2017-05-28'],['2017-05-12','2017-06-11']]
            // ],
            // "getAccount3" => 
            // [
            //     "in_file" => "/tmp/users_0609.csv",
            //     "out_file" => "/tmp/data_0609.csv",
            //     "sep_date" => "2017-06-09",
            //     "date_range" =>[['2017-05-08','2017-06-07'],['2017-06-09','2017-07-09'],['2017-06-17','2017-07-16']]
            // ]
        ];
        // 获取三种玩家
        $micro_db = DBManager::factory("hon_micro");
        foreach ($fun_arr as $fun_name => $value) {

            $users_txt = fopen($value['in_file'], "r");
            $range = $value['date_range'];
            $sep_date = $value['sep_date'];

            $out_csv = fopen($value['out_file'], "a+");
            $header_title = ['account_id'];
            foreach ($range as $date_arr) {
                $header_title[] = $date_arr[0] . "~" . $date_arr[1];
            }
            fputcsv($out_csv, $header_title);
            while (!feof($users_txt)) {
                $num ++;
                $line = trim(fgets($users_txt));
                if (!$line) {
                    continue ;
                }
                $account_id = $line;
                $out = [];
                echo "$fun_name : $num $account_id" . PHP_EOL;

                $account_stmt = $micro_db->prepare("select super_id,garena_id from hon_accounts.account where account_id=:id");
                $account_stmt->execute(['id' => $account_id]);
                $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
                $super_id = $account['super_id'];

                // $topup_type = $this->getPlayerTopupType($super_id, $sep_date);
                $consume_points_arr = $this->getPlayerConsume($super_id, $range);
                $out[] = $account_id;
                // $out['topup_type'] = $topup_type;
                for ($i=0; $i < count($range); $i++) { 
                    $out[] = $consume_points_arr[$i];
                }
                // 写入文件
                fputcsv($out_csv, $out);
            }
            fclose($users_txt);
        }
        Logger::info(get_class() . ": job run finish.");
    }


    public function getPlayerTopupType($super_id, $sep_date)
    {
        $micro_db = DBManager::factory("hon_micro");
        $invoice_stmt = $micro_db->prepare("select * from invoice where super_id=:super_id and (type in ('shell','airpay','counter','gop')  or (type='' and gifter_nick='Reward API')) order by invoice_id asc limit 1");
        $invoice_stmt->execute(['super_id' => $super_id]);
        $invoice = $invoice_stmt->fetch(PDO::FETCH_ASSOC);
        $has_topup = false;
        if ($invoice) {
            $has_topup = true;
            if (strtotime($invoice['date'] . " " . $invoice['time']) < strtotime($sep_date)) {
                return 1;
            }
        }

        $archive_stmt = $micro_db->prepare("select * from invoice_archives where super_id=:super_id and (type in ('shell','airpay','counter','gop')  or (type='' and gifter_nick='Reward API')) order by invoice_id asc limit 1");
        $archive_stmt->execute(['super_id' => $super_id]);
        $archive = $archive_stmt->fetch(PDO::FETCH_ASSOC);
        if ($archive) {
            $has_topup = true;
            if (strtotime($archive['date'] . " " . $archive['time']) < strtotime($sep_date)) {
                return 1;
            }
        }
        if (!$invoice && !$archive) {
            return 0;
        }
        return 2;
    }

    public function getPlayerConsume($super_id, $data_range_arr)
    {
        $result = [];
        foreach ($data_range_arr as $value) {
            $start_date = $value[0];
            $end_date = $value[1];
            $result[] = $this->getPlayerConsumePoints($super_id, $start_date, $end_date);
        }
        return $result;
    }

    private function getPlayerConsumePoints($super_id, $start_date, $end_date)
    {
        $micro_db = DBManager::factory("hon_micro");
        $trans_stmt = $micro_db->prepare("select sum(spent) total from transactions where super_id=:id and `date`>=:start_date and `date` <=:end_date and status=1");
        $trans_stmt->execute(['id' => $super_id, 'start_date' => $start_date, 'end_date' => $end_date]);
        $result = $trans_stmt->fetch(PDO::FETCH_ASSOC);
        return intval($result['total']);
    }


}

$clazz = new CoinsData();
$clazz->run();
