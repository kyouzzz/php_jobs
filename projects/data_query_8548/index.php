<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class DataQuery
{

    const PLAYER_NUM = 5000;

    public function run()
    {
        $fun_arr = [
            "getOldPlayers" => "/tmp/data_old.csv",
            "getNewPlayers" => "/tmp/data_new.csv",
            "getAwayPlayers" => "/tmp/data_away.csv",
        ];
        // 获取三种玩家
        foreach ($fun_arr as $fun_name => $file_name) {
            echo date("Y-m-d H:i:s") . " start $fun_name" . PHP_EOL;
            $players = $this->$fun_name();
            echo date("Y-m-d H:i:s") . " run $fun_name" . PHP_EOL;
            $out_csv = fopen($file_name, "a+");
            fputcsv($out_csv, ['garena_id', 'account_id', 'days_2016h1', 'days_2017h1', 
                'coins_2016h1', 'coins_2017h1', 'foc_2016h1', 'foc_2017h1', 'mid_2016h1', 'mid_2017h1']);
            foreach ($players as $account_id) {
                echo "$fun_name : $account_id" . PHP_EOL;
                $out = $this->getPlayerData($account_id);
                // 写入文件
                fputcsv($out_csv, $out);
                sleep(1);
            }
        }
        Logger::info(get_class() . ": job run finish.");
    }

    public function getPlayerData($account_id)
    {
        $result = [];
        // 获取账号信息
        $micro_db = DBManager::factory("hon_micro");
        $account_stmt = $micro_db->prepare("select super_id,garena_id from hon_accounts.account where account_id=:id");
        $account_stmt->execute(['id' => $account_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        $super_id = $account['super_id'];
        $result['garena_id'] = $account['garena_id'];
        $result['account_id'] = $account_id;
        // 获取登录天数
        $result['days_2016h1'] = $this->getUserLoginDays($account_id, "2016-01-01", "2016-06-30", true);
        $result['days_2017h1'] = $this->getUserLoginDays($account_id, "2017-01-01", "2017-06-30", false);
        // 获取 coins
        $result['coins_2016h1'] = $this->getPlayerConsumePoints($super_id, "2016-01-01", "2016-06-30");
        $result['coins_2017h1'] = $this->getPlayerConsumePoints($super_id, "2017-01-01", "2017-06-30");
        // 获取 foc
        $result['foc_2016h1'] = $this->focCount($account_id, "2016-01-01", "2016-06-30");
        $result['foc_2017h1'] = $this->focCount($account_id, "2017-01-01", "2017-06-30");
        // 获取 mid
        $result['mid_2016h1'] = $this->midCount($account_id, "2016-01-01", "2016-06-30");
        $result['mid_2017h1'] = $this->midCount($account_id, "2017-01-01", "2017-06-30");
        return $result;
    }

    private function getOldPlayers()
    {
        $table = "logins_20161020";
        $db = "hon_tracking_bak";
        $micro_db = DBManager::factory($db);
        $result = [];
        $num = 0;
        while (true) {
            // 2016.1.1~2016.6.30 login_id  993104555,1157509698
            $rand_id = rand(993104555,1157509698);
            $login_stmt = $micro_db->prepare("select account_id from $table where login_id=:rand_id");
            $login_stmt->execute(["rand_id" => $rand_id]);
            $row = $login_stmt->fetch(PDO::FETCH_ASSOC);
            if (!in_array($row['account_id'], $result)) {
                $result[] = $row['account_id'];
                $num ++ ;
                if ($num >= self::PLAYER_NUM) {
                    return $result;
                }
            }
        }
    }
    private function getNewPlayers()
    {
        $result = [];
        $num = 0;
        while (1) {
            // 2016.6.30~2016.12.31  30881880~32155355
            $rand_account_id = rand(30881880,32155355);
            if (!in_array($rand_account_id, $result)) {
                $result[] = $rand_account_id;
                $num ++ ;
                if ($num >= self::PLAYER_NUM) {
                    return $result;
                }
            }
        }
    }
    private function getAwayPlayers()
    {
        $result = [];
        $micro_db = DBManager::factory("hon_tracking");
        $num = 0;
        while (true) {
            // 2016.12.1~2017.1.13 login_id 36197405,68886082
            $rand_id = rand(36197405,68886082);
            $login_stmt = $micro_db->prepare("select account_id from logins where login_id=:rand_id");
            $login_stmt->execute(["rand_id" => $rand_id]);
            $row = $login_stmt->fetch(PDO::FETCH_ASSOC);
            $account_id = $row['account_id'];
            $days = $this->getUserLoginDays($account_id, "2017-01-01", "2017-06-30", false);
            if (!$days && !in_array($account_id, $result)) {
                $result[] = $account_id;
                $num ++;
                if ($num >= self::PLAYER_NUM) {
                    return $result;
                }
            }
            
        }
    }

    private function getUserLoginDays($account_id, $start_date, $end_date, $use_bak)
    {
        if (!$use_bak) {
            $table = "logins";
            $db = "hon_tracking";
        } else {
            $table = "logins_20161020";
            $db = "hon_tracking_bak";
        }
        $micro_db = DBManager::factory($db);
        $login_stmt = $micro_db->prepare("select count(distinct date) as days from $table where account_id=:account_id and `date`>=:start_date and `date`<=:end_date");
        $login_stmt->execute(['account_id'=> $account_id, "start_date" => $start_date, "end_date" => $end_date]);
        $result = $login_stmt->fetch(PDO::FETCH_ASSOC);
        return $result['days'];
    }


    private function getPlayerConsumePoints($super_id, $start_date, $end_date)
    {
        $micro_db = DBManager::factory("hon_micro");
        $trans_stmt = $micro_db->prepare("select sum(spent) total from transactions where super_id=:id and `date`>=:start_date and `date` <=:end_date and status=1");
        $trans_stmt->execute(['id' => $super_id, 'start_date' => $start_date, 'end_date' => $end_date]);
        $result = $trans_stmt->fetch(PDO::FETCH_ASSOC);
        return intval($result['total']);
    }

    private function focCount($account_id, $start_date, $end_date)
    {
        $micro_db = DBManager::factory("hon_stats");
        $foc_stmt = $micro_db->prepare("
            select count(1) total from match_stats t1, match_summ t2
            where t1.match_id=t2.match_id
            and t2.map='caldavar'
            and t2.mdt >= :start_date
            and t2.mdt <= :end_date
            and t2.time_played > 0
            and t1.hero_id > 0
            and t1.account_id = :account_id
        ");
        $foc_stmt->execute(['account_id' => $account_id, 'start_date' => $start_date, 'end_date' => $end_date]);
        $result = $foc_stmt->fetch(PDO::FETCH_ASSOC);
        return intval($result['total']);
    }

    private function midCount($account_id, $start_date, $end_date)
    {
        $micro_db = DBManager::factory("hon_stats");
        $mid_stmt = $micro_db->prepare("
            select count(1) total from midwars_match_stats t1, match_summ t2
            where t1.match_id=t2.match_id
            and t2.mdt >= :start_date
            and t2.mdt <= :end_date
            and t2.time_played > 0
            and t1.hero_id > 0
            and t1.account_id = :account_id
        ");
        $mid_stmt->execute(['account_id' => $account_id, 'start_date' => $start_date, 'end_date' => $end_date]);
        $result = $mid_stmt->fetch(PDO::FETCH_ASSOC);
        return intval($result['total']);   
    }

}

$clazz = new DataQuery();
$clazz->run();