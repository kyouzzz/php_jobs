<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class HonDayReward
{
    const LIMIT_NUM = 100;

    public function run()
    {
        $handle = fopen("data/users.csv","r");
        $storage = [];
        $num = 0;
        $file_idx = 1;
        while (!feof($handle)) {
            $line = trim(fgets($handle));
            if (!$line) {
                continue ;
            }
            $num ++;
            echo "Num $num" . PHP_EOL;
            $account_id = $line;
            $storage[] = $account_id;
            if (count($storage) == static::LIMIT_NUM) {
                $file_idx = ceil($num/3000);
                $this->handlePlayers($storage, $file_idx);
                $storage = [];
            }
        }
        $this->handlePlayers($storage,$file_idx);
        Logger::info(get_class() . ": job run finish.");
    }

    public function handlePlayers($account_list, $file_idx)
    {
        $revert_sql_file = "data/revert_sql_{$file_idx}.sql";
        $update_sql_file = "data/update_sql_{$file_idx}.sql";

        $standing_list = [];
        // 分批处理 LIMIT_NUM 个玩家
        foreach ($account_list as $account_id) {
            // get account standing
            $account_standing = $this->getAccountStanding($account_id);
            if ($account_standing === false) {
                continue ;
            }
            if ($account_standing == 3) {
                continue ;
            }
            $standing_list[$account_standing][] = $account_id;
        }
        // write update sql
        $update_sql = $this->buildeUpdateSql(3, $account_list) . PHP_EOL;
        file_put_contents($update_sql_file, $update_sql, FILE_APPEND);
        // write revert sql
        foreach ($standing_list as $standing_before => $account_arr) {
            $revert_sql = $this->buildeUpdateSql($standing_before, $account_arr) . PHP_EOL;
            file_put_contents($revert_sql_file, $revert_sql, FILE_APPEND);
        }

    }

    public function getAccountStanding($account_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $account_stmt = $micro_db->prepare("select super_id,garena_id,account_id,standing from hon_accounts.account where account_id=:id");
        $account_stmt->execute(['id' => $account_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        if ($account) {
            return $account['standing'];
        }
        return false;
    }

    public function buildeUpdateSql($standing, $account_ids)
    {
        $sql = "update hon_accounts.account set standing={$standing} where account_id in (" . implode(",", $account_ids). ");";
        return $sql;
    }

}

$clazz = new HonDayReward();
$clazz->run();