<?php

class DBManager
{
    public function getPDO($conf)
    {
        $host = $conf['host'];
        $port = $conf['port'];
        $user = $conf['user'];
        $password = $conf['password'];
        $dbname = $conf['dbname'];
        try {
            $new_pdo = new PDO(
                "mysql:host=$host;dbname=$dbname;port=$port",
                $user,
                $password);
            $new_pdo->query("SET NAMES utf8");
            $new_pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            return $new_pdo;
        } catch (PDOException $e) {
            print_r($e->getMessage());
        }
    }
}

Class JobClass
{

    public function run()
    {
        $arr = ["Item_Lightning2"];
        // $arr = ["Item_FrostfieldPlate","Item_IoynStone"];
        foreach ($arr as $v) {
            $this->runItem($v, $arr);
        }
    }

    public function runItem($str, $arr)
    {
        $conf['host'] = '127.0.0.1';
        $conf['port'] = '3306';
        $conf['user'] = 'root';
        $conf['password'] = 'jingle@100';
        $conf['dbname'] = 'data';

        $dbm = new DBManager();
        $pdo = $dbm->getPDO($conf);
        $stmt = $pdo->prepare("select * from guides where guidedata like :Item "); //where guidedata like '%?%'
        $stmt->execute(['Item'=> "%$str%"]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $guide) {
            $data = unserialize($guide['guidedata']);
            $items = $data['items'];
            if (!$items) {
                continue;
            }
            foreach ($items as &$v) {
                foreach ($v as $ck => $cv) {
                    if (in_array($cv, $arr)) {
                        array_splice($v, $ck, 1);
                        $v[] = '';
                    }
                }
            }
            $data['items'] = $items;
            $new_ser = addslashes(serialize($data));
            $guide_id = $guide['guide_id'];

            $update_sql = "update guides set guidedata = '" . $new_ser ."' where guide_id=$guide_id;" 
                . PHP_EOL . PHP_EOL;

            file_put_contents("/tmp/remove_item.sql", $update_sql, FILE_APPEND);
        }
    }

}

$job = new JobClass();
$job->run();
