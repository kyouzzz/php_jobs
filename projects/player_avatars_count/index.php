<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class PlayerAvatarsCount
{

    public function run()
    {
        $out_csv = fopen("/tmp/users_avatars.csv", "a+");
        fputcsv($out_csv, ['garena_id', 'super id', 'login days', 'total points', 'total avatars']);

        $tracking_db = DBManager::factory("hon_tracking");
        $micro_db = DBManager::factory("hon_micro");
        $min_id = 0;
        while (1) {
            // 或者近三个月活跃的玩家以及登陆天数
            $login_stmt = $tracking_db->prepare("select account_id, count(distinct date) as days from logins WHERE login_id > 123269019 and account_id >:min_id group by account_id order by account_id asc limit 10000");
            $login_stmt->execute(['min_id' => $min_id]);
            $player_logins = $login_stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($player_logins) == 0) {
                Logger::info(get_class() . ": job run finish.");
                return ;
            }

            foreach ($player_logins as $value) {
                $csv_column = [];
                $account_id = $value['account_id'];

                // find account
                $account_stmt = $micro_db->prepare("select super_id,garena_id from hon_accounts.account where account_id=:id");
                $account_stmt->execute(['id' => $account_id]);
                $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
                $super_id = $account['super_id'];
                // 查询玩家的消耗金币数量
                $points = $this->getPlayerConsumePoints($super_id);
                // 查询玩家的皮肤数量
                $avatar_count = $this->getPlayerAvatarCount($super_id);
                // 记录到文件
                $csv_column['garena_id'] = $account['garena_id'];
                $csv_column['super_id'] = $super_id;
                $csv_column['login_days'] = $value['days'];
                $csv_column['points'] = $points;
                $csv_column['avatar_count'] = $avatar_count;
                echo "Runing : " . ($i ++ ) . PHP_EOL;
                fputcsv($out_csv, $csv_column);
                $min_id = $account_id;
            }
        }


    }

    private function getPlayerConsumePoints($super_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $trans_stmt = $micro_db->prepare("select sum(spent) total from transactions where super_id=:id and `date`>=:date and status=1");
        $trans_stmt->execute(['id' => $super_id, 'date' => '2017-04-14']);
        $result = $trans_stmt->fetch(PDO::FETCH_ASSOC);
        return intval($result['total']);
    }

    private function getPlayerAvatarCount($super_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $upgrades_stmt = $micro_db->prepare("select count(1) total from upgrades left join products using (product_id) where super_id=:id and products.type='Alt Avatar'");
        $upgrades_stmt->execute(['id' => $super_id]);
        $result = $upgrades_stmt->fetch(PDO::FETCH_ASSOC);
        return $result['total'];
    }

}

$clazz = new PlayerAvatarsCount();
$clazz->run();