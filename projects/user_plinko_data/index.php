<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class UserPlinkoData
{

    private $config_arr = null;
    private $check_product_arr = [5150,3579,3185,2755,2083,1360,1043];

    public function run()
    {
        $input_file = fopen("data/plinko.csv","r");
        $num = 0;
        while (!feof($input_file)) {
            $line = trim(fgets($input_file));
            if (!$line) {
                continue ;
            }
            $num ++;
            echo "Num $num" . PHP_EOL;
            $line_arr = explode(",", $line);
            $account_id = trim($line_arr[0]);
            if (!is_numeric($account_id)) {
                continue;
            }
            $currency = intval($line_arr[1]); // 1 gold 2 ticket
            $time = trim($line_arr[2]);
            $config_id = intval($line_arr[4]);
            $this->handleDropData($account_id, $currency, $time, $config_id);
        }
        Logger::info(get_class() . ": job run finish.");
    }

    public function handleDropData($account_id, $currency, $date_time, $config_id)
    {
        // check inserted
        $exist = $this->checkHandled($account_id);
        if ($exist) {
            return false;
        }
        $configs = $this->getPlinkoConfig();
        if (empty($configs[$config_id]['gold_cost'])) {
            return false;
        }
        $points = $configs[$config_id]['gold_cost'];
        // get account
        $account = $this->getAccount($account_id);
        $super_id = $account['super_id'];
        $nickname = $account['nickname'];
        // calculate gold summary
        $plinko_data['account_id'] = $account_id;
        $plinko_data['nickname'] = $nickname;
        $plinko_data['points1'] = $this->getPlayerPoints($super_id, '2017-10-05 00:00:00', '2017-12-06 23:59:59');
        $plinko_data['points2'] = $this->getPlayerPoints($super_id, '2017-12-07 00:00:00', '2017-12-11 23:59:59');
        $plinko_data['plinko_points1'] = $this->getDropPoints($account_id, '2017-10-05 00:00:00', '2017-12-06 23:59:59');
        $plinko_data['plinko_points2'] = $this->getDropPoints($account_id, '2017-12-07 00:00:00', '2017-12-11 23:59:59');
        $plinko_data = array_merge($plinko_data, $this->getCheckProducts($super_id));
        echo "insert $account_id plinko data" . PHP_EOL;
        $this->insertPlinkoData($plinko_data);
    }

    private function getCheckProducts($super_id)
    {
        $in_str = implode(",", $this->check_product_arr);
        $micro_db = DBManager::factory("hon_micro");
        $player_products_stmt = $micro_db->prepare("select * from upgrades where super_id=:super_id and product_id in ($in_str)");
        $player_products_stmt->execute(['super_id'=> $super_id]);
        $player_products = $player_products_stmt->fetchAll(PDO::FETCH_ASSOC);
        $has_pid = [];
        foreach ($player_products as $value) {
            $pid = $value['product_id'];
            $has_pid[] = $pid;
        }
        $result = [];
        foreach ($this->check_product_arr as $value) {
            $result["p$value"] = in_array($value, $has_pid) ? 1 : 0;
        }
        return $result;
    }

    private function getPlayerPoints($super_id, $start_time, $end_time)
    {
        $micro_db = DBManager::factory("hon_micro");
        $trans_stmt = $micro_db->prepare("select sum(spent) spent from transactions where CONCAT(date,' ',time) >=:start_time and CONCAT(date,' ',time) <=:end_time and status=1 and super_id=:super_id");
        $trans_stmt->execute(['super_id' => $super_id, 'start_time' => $start_time, 'end_time' => $end_time]);
        $total = $trans_stmt->fetch(PDO::FETCH_ASSOC);
        return intval($total['spent']);
    }

    private function getAccount($account_id)
    {
        $micro_db = DBManager::factory("hon_micro");
        $account_stmt = $micro_db->prepare("select super_id,garena_id,account_id,nickname from hon_accounts.account where account_id=:id");
        $account_stmt->execute(['id' => $account_id]);
        $account = $account_stmt->fetch(PDO::FETCH_ASSOC);
        return $account;
    }

    private function checkHandled($account_id)
    {
        $data_db = DBManager::factory("local_db");
        $stmt = $data_db->prepare("select *  from test.plinko_data where account_id=:id");
        $stmt->execute(['id' => $account_id]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    private function updatePlinkoData($account_id, $points, $time)
    {
        if (empty($points)) {
            return false;
        }
        $data_db = DBManager::factory("local_db");
        if (strtotime($time) <= strtotime('2017-12-6 23:59:59')) {
            $update_column = "plinko_points1";
        } else {
            $update_column = "plinko_points2";
        }
        $update_sql = "update test.plinko_data set $update_column=$update_column+$points where account_id=:id";
        $stmt = $data_db->prepare($update_sql);
        $stmt->execute(['id' => $account_id]);
        return $stmt->rowCount();
    }

    private function insertPlinkoData($data)
    {
        $data_db = DBManager::factory("local_db");
        $insert_sql = "insert into test.plinko_data (account_id,nickname,points1,points2,plinko_points1,plinko_points2,p5150,p3579,p3185,p2755,p2083,p1360,p1043) values ({$data['account_id']},'{$data['nickname']}',{$data['points1']},{$data['points2']},{$data['plinko_points1']},{$data['plinko_points2']},{$data['p5150']},{$data['p3579']},{$data['p3185']},{$data['p2755']},{$data['p2083']},{$data['p1360']},{$data['p1043']})";
        $insert_stmt = $data_db->prepare($insert_sql);
        $insert_stmt->execute();
        $result = $insert_stmt->rowCount();
        return $result;
    }

    private function getPlinkoConfig()
    {
        if (!$this->config_arr) {
            $micro_db = DBManager::factory("hon_micro");
            $plinko_config = $micro_db->prepare("select * from hon_micro.casino_config");
            $plinko_config->execute();
            $configs = $plinko_config->fetchAll(PDO::FETCH_ASSOC);
            $result = [];
            foreach ($configs as $key => $value) {
                $id = $value['id'];
                $result[$id]['gold_cost'] = $value['gold_cost'];
                $result[$id]['ticket_cost'] = $value['ticket_cost'];
            }
            $this->config_arr = $result;
        }
        return $this->config_arr;
    }

    private function getDropPoints($account_id, $start_time, $end_time)
    {
        $sql = "select count(1) total from hon_tracking.casino_drop_history where id > 158196218 and id < 161500052 and 
        status_code=1 and currency=1  and time>=:start_time and time <:end_time and user_id=:id; ";
        $stats_db = DBManager::factory("hon_stats");
        $stmt = $stats_db->prepare($sql);
        $stmt->execute(['start_time'=>$start_time, 'end_time'=>$end_time, 'id' => $account_id]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return intval($result['total']) * 45;
    }
}

$clazz = new UserPlinkoData();
$clazz->run();