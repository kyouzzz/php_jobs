<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class NaeuInvoiceJob
{
    private $micro_db;

    public function run()
    {
        $micro_db = DBManager::factory("hon_micro");
        $max_id = 55608397; // 2016-06-20
        $limit = 10000;

        $has_more = true;
        while ($has_more) {
            $stmt = $micro_db->prepare("delete from invoice where invoice_id < :id  limit :limit");
            $stmt->execute(['id' => $max_id, 'limit' => $limit]);
            $result = $stmt->rowCount();
            Logger::info(get_class() . ": Delete number $result");
            if ($result < $limit) {
                $has_more = false;
            }
            sleep(10);
        }
        Logger::info(get_class() . ': Naeu job run finish.');
    }

}

$clazz = new NaeuInvoiceJob();
$clazz->run();
