<?php
require __DIR__ . "/../include.php";
use Lib\DBManager;
use Lib\Logger;

class SeaInvoiceJob
{
    private $micro_db;

    public function run()
    {
        // Rename invocie => invoice_archives
        // Create invoice; insert into invocie (select * from invoice_archives where invoice_archives)
        $micro_db = DBManager::factory("hon_micro");

        $end_date = "2016-06-20";
        $curr_date = date("Y-m-d");
        // invoice_archives data => invoice
        $has_more = true;
        while ($has_more) {
            echo $curr_date . PHP_EOL;
            // remove
            $remove_stmt = $micro_db->prepare("insert into invoice (select * from invoice_archives where date=:date)");
            $remove_stmt->execute(['date' => $curr_date]);
            $insert_num = $remove_stmt->rowCount();
            if ($insert_num) {
                // insert
                $delete_stmt = $micro_db->prepare("delete from invoice_archives where date=:date");
                $delete_stmt->execute(['date' => $curr_date]);
                $delete_num = $delete_stmt->rowCount();
                if ($delete_num) {
                    Logger::info(get_class() . ": $curr_date transfer success.");
                } else {
                    Logger::error(get_class() . ": $curr_date transfer fail.");
                }
            }
            $curr_date = date("Y-m-d", strtotime($curr_date . " -1 day"));
            if ($curr_date <= $end_date) {
                $has_more = false;
            }
            sleep(5);
        }
        Logger::info(get_class() . ': Sea job run finish.');
    }

}

$clazz = new SeaInvoiceJob();
$clazz->run();